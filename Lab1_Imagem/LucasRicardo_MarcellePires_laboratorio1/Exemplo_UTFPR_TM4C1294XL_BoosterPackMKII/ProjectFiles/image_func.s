		AREA    |.text|, CODE, READWRITE
		
; Import de variáveis 		
		IMPORT MyBinaryImage1
		IMPORT MyBinaryImage1_length
		IMPORT MyBinaryImage2
		IMPORT MyBinaryImage2_length
			
; Declaração de globais
		GLOBAL ReadImage1
		GLOBAL ReadImage2
		GLOBAL Start_asm
		GLOBAL RightImage
		GLOBAL LeftImage
		GLOBAL UpImage
		GLOBAL DownImage

;Função que inicia os ponteiros
;=============================================
Start_asm
		LDR R5,=MyBinaryImage1
		ADD R5,#58 ;offset header
		LDR R6,=MyBinaryImage2
		ADD R6,#58 ;offset header
		BX LR

;Leitura de imagem
;=============================================
ReadImage1
		LDRB R0,[R5],#3
		BX LR 
		
ReadImage2
		LDRB R0,[R6],#3
		BX LR 

;Movimentação para a direita
;=============================================	
RightImage
		LDR R6,=MyBinaryImage2
		MOV R2,#0x4837 ;Contador - Tamanho da imagem - 18.490
		ADD R6,R2
		
		LDRB R1,[R6]
		PUSH {R1}
		
		MOV R3,#1
		MOV R7,#6144
loopright		
		LDRB R1,[R6,#-3]
		STRB R1,[R6],#-3
		ADD R3,#1
		CMP R3,R7
		BNE loopright
		POP{R1}
		STRB R1,[R6]
		BX LR		

;Movimentação para a esquerda
;=============================================
LeftImage
		LDR R6,=MyBinaryImage2
		ADD R6,#58 ;offset header
	
		LDRB R1,[R6] ; Salva a info do primeiro pixel
		PUSH {R1}	 ; Guarda na pilha

		MOV R3,R6 	 ; Backup posicao 1
		MOV R2,#1 	 ; Contador
		MOV R7,#6144

loopleft		
		LDRB R1,[R6,#3]
		STRB R1,[R6],#3
		ADD R2,#1 ; incrementa o contador	
		CMP R2,R7 ; Chegou no fim do vetor?
		BNE loopleft
		
		POP {R1}	; Retorna o primeiro pixel ao ultimo ponto
		STRB R1,[R6]
		
		BX LR

;Movimentação para cima
;=============================================
UpImage	
		PUSH{R0,R1,R2}

;Salvando a primeira linha
		LDR R0,=MyBinaryImage1
		ADD R0,#58
		MOV R2,#1
salva_linha
		LDRB R1,[R0],#3
		PUSH {R1} 
		ADD R2,#1
		CMP R2,#96
		BNE salva_linha

;Chamada de movimentação
		LDR R0,=MyBinaryImage1
		ADD R0,#58
		MOV R3,#6048
		MOV R2,#1
move_pixel
		LDRB R1,[R0,#288]
		STRB R1,[R0],#3
		ADD R2,#1
		CMP R2,R3
		BNE move_pixel

;Descarrega no final da linha
		LDR R0,=MyBinaryImage1
		MOV R1,#0x4837
		ADD R0,R1
		MOV R2,#1

descarrega_linha
		POP {R1}
		STRB R1,[R0],#-3
		ADD R2,#1
		CMP R2,#96
		BNE descarrega_linha

		POP {R0,R1,R2}
		BX LR

;Movimentação para baixo
;=============================================
DownImage
		PUSH{R0,R1,R2}
		
;Carrega o final da linha
		LDR R0,=MyBinaryImage1
		MOV R1,#0x4837
		ADD R0,R1
		MOV R2,#1

descarrega_linha_baixo
		LDRB R1,[R0],#-3
		PUSH {R1}
		ADD R2,#1
		CMP R2,#96
		BNE descarrega_linha_baixo

; chamada de movimentação
		LDR R0,=MyBinaryImage1
		MOV R1,#0x4837 ; Final da imagem
		ADD R0,R1
		MOV R2,#1 
		
move_pixel_baixo
		SUB R0,#288
		LDRB R1,[R0]
		ADD R0,#288
		STRB R1,[R0],#-3
		ADD R2,#1
		CMP R2,#6048
		BNE move_pixel_baixo

;Descarregando na ultima linha
		LDR R0,=MyBinaryImage1
		ADD R0,#58
		MOV R2,#1
salva_linha_baixo
		POP {R1} 
		STRB R1,[R0],#3
		ADD R2,#1
		CMP R2,#96
		BNE salva_linha_baixo

		POP {R0,R1,R2}
		BX LR

		ALIGN
		END
