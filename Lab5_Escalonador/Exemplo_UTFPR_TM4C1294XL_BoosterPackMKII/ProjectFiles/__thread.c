/* Include standards */
#include "cmsis_os.h"
#include "TM4C129.h" // Device header
#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <string.h>
#include <math.h>

/* Include drivers */
#include "grlib/grlib.h"
#include "driverlib/timer.h"
#include "inc/hw_ints.h"
#include "cfaf128x128x16.h"
#include "display.h"
#include "buzzer.h"
#include "uart.h"
#include "driverlib/pwm.h"
#include "led.h"
#include "servo.h"

#define SIMULADOR 0
#define GANTT 0

#define IDEIA_CRIA_THRED 0

// MACROS
/* Timer */
#define TIMER_TICK 5
#define TIMER_CICLOS 1200

/* Movimentacao Braco */
#define MIN 0
#define MAX 65000

/* Threads */
#define N_THREADS 4
#define AJUSTE_PRIORIDADE 10

/* Estados das tarefas */
#define EXECUTANDO 0
#define SUSPENSA 1
#define PRONTA 2
#define ENCERRADA 3
#define NAO_CRIADA 5

/* Status de Encerramento */
#define MASTER_FAULT 0
#define SECONDARY_FAULT_1 1 // Encerramento apos o deadline
#define SECONDARY_FAULT_2 2 // Encerramento antes da metade do delta_l
#define NH 3 								// Nothing Happened

/* Niveis de Prioridade */
#define REAL_TIME -100
#define HIGH -30
#define NORMAL 0
#define LOW 10

/* Fibonacci */
#define PERIODO_FIBO 1000 // 1000ms - 1000000us
#define PRIORIDADE_FIBO 0
#define EXECUCAO_FIBO 20000
#define FIBO 0

/* Primo */
#define PERIODO_PRIMO 200 // 200ms - 200000us
#define PRIORIDADE_PRIMO -100 
#define EXECUCAO_PRIMO 24000
#define PRIMO 1

/* Manipulador */
#define PERIODO_MAN 100
#define PRIORIDADE_MAN 10
#define EXECUCAO_MAN 10000
#define MANIPULADOR 2 

/* UART */
#define UART 3 
#define EXECUCAO_UART 1000
#define PRIORIDADE_UART -30 //High

/* UART Mutex */
osMutexDef (uart_mutex);
osMutexId (mutex_UART_ID);

/* Timer */
void timer_callback(void const *arg);	
osTimerDef(Timer,timer_callback);
osTimerId Timer_ID;

/* Definicao dos Mails*/
// Mail utilizado pelo UART
// Mensagem UART
osMailQId mid_UART_ID;
osMailQDef (MailQueue, 5, char);

//Defini��o das filas
// Msg plot
osMessageQId(display_queue_ID);
osMessageQDef(display_queue, 5, uint8_t);

/* Cria��o das threads*/
void gera_primo			(void const *argument);
void gera_fibonacci (void const *argument);
void manipulador		(void const *argument);
void escalonador		(void const *argument);
void uart						(void const *argument);

// Vari�vel que determina ID das threads
osThreadId gera_fibonacci_ID;
osThreadId gera_primo_ID;
osThreadId escalonador_ID; 
osThreadId manipulador_ID;
osThreadId uart_ID; 

// Escalonador alta prioridade
// Timer com prioridade maior que escalonador.
osThreadDef (escalonador, osPriorityHigh, 1, 0); 

// Tarefas normais
osThreadDef (gera_primo, osPriorityNormal, 1, 0); 
osThreadDef (gera_fibonacci, osPriorityNormal, 1, 0);     
osThreadDef (manipulador, osPriorityNormal, 1, 0);     
osThreadDef (uart, osPriorityNormal, 1, 0);    

/*------------------------------------------------------------*/
// Estrutura Thread
/*------------------------------------------------------------*/
typedef struct Thread_info{
	osThreadId id;					 // Thread ID
	int8_t  prioridade; 		 // Prioridade da thread
	uint64_t delta_l;  			 // Delta de relaxamento
	uint64_t tempo_inicio; 	 // Tempo de inicio da thread
	uint64_t tempo_fim; 	   // Tempo de fim da thread
	uint64_t prazo_estimado; // Tempo de execucao
	uint64_t deadline; 			 // Deadline
	uint8_t  estado; 				 // Estado da tarefa
	uint8_t  end_status; 		 // Status de encerramento
}thread_info;

/*------------------------------------------------------------*/
// VARIAVEIS GLOBAIS
/*------------------------------------------------------------*/
	//Vetor de controle de threads
	thread_info thread[N_THREADS];

	//Variaveis de teste
	char n_string[20];
	char cycles_char[30];
	uint32_t cycles,cycles_ini,cycles_fim,delta_cycle;
	float tempo;

	//Manipulador
	bool mudar_peca = false;

	//UART Interrupt
	bool UART_interrupt = false;
/*------------------------------------------------------------*/
// Timer Callback
/*------------------------------------------------------------*/
void timer_callback(void const *arg){	
	// Seta o sinal do escalonador e apaga qualquer outro sinal de tarefas
	osSignalSet(escalonador_ID,0x01);

}	
/*------------------------------------------------------------*/
// Verifica Primo
/*------------------------------------------------------------*/
int isPrime(uint32_t number){
		uint32_t i;
	
    if(number < 2) return 0;
    if(number == 2) return 1;
    if(number % 2 == 0) return 0;
    for(i=3; (i*i)<=number; i+=2){
        if(number % i == 0 ) return 0;
    }
    return 1;
}
/*------------------------------------------------------------*/
// Configuracao do CYCCNT
/*------------------------------------------------------------*/
void DWT_Config(){
	/* DWT (Data Watchpoint and Trace) registers, only exists on ARM Cortex with a DWT unit */
	
	/*!< DWT Control register */
	#define KIN1_DWT_CONTROL             (*((volatile uint32_t*)0xE0001000))
	
	/*!< CYCCNTENA bit in DWT_CONTROL register */
	#define KIN1_DWT_CYCCNTENA_BIT       (1UL<<0)
	
	/*!< DWT Cycle Counter register */
	#define KIN1_DWT_CYCCNT              (*((volatile uint32_t*)0xE0001004))
	
	/*!< DEMCR: Debug Exception and Monitor Control Register */
	#define KIN1_DEMCR                   (*((volatile uint32_t*)0xE000EDFC))
	
	/*!< Trace enable bit in DEMCR register */
	#define KIN1_TRCENA_BIT              (1UL<<24)
	
	/*!< TRCENA: Enable trace and debug block DEMCR 
		(Debug Exception and Monitor Control Register */
	#define InitCycleCounter() \
		KIN1_DEMCR |= KIN1_TRCENA_BIT
	
	/*!< Reset cycle counter */
	#define ResetCycleCounter() \
		KIN1_DWT_CYCCNT = 0
	
	/*!< Enable cycle counter */
	#define EnableCycleCounter() \
		KIN1_DWT_CONTROL |= KIN1_DWT_CYCCNTENA_BIT
	
	/*!< Disable cycle counter */
	#define DisableCycleCounter() \
		KIN1_DWT_CONTROL &= ~KIN1_DWT_CYCCNTENA_BIT
	
	/*!< Read cycle counter register */
	#define GetCycleCounter() \
		KIN1_DWT_CYCCNT
}


/*------------------------------------------------------------*/
// Numeros para string
/*------------------------------------------------------------*/
static void intToString(int64_t value, char * pBuf, uint32_t len, uint32_t base, uint8_t zeros){
	static const char* pAscii = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ";
	bool n = false;
	int pos = 0, d = 0;
	int64_t tmpValue = value;

	// the buffer must not be null and at least have a length of 2 to handle one
	// digit and null-terminator
	if (pBuf == NULL || len < 2)
			return;

	// a valid base cannot be less than 2 or larger than 36
	// a base value of 2 means binary representation. A value of 1 would mean only zeros
	// a base larger than 36 can only be used if a larger alphabet were used.
	if (base < 2 || base > 36)
			return;

	if (zeros > len)
		return;
	
	// negative value
	if (value < 0)
	{
			tmpValue = -tmpValue;
			value    = -value;
			pBuf[pos++] = '-';
			n = true;
	}

	// calculate the required length of the buffer
	do {
			pos++;
			tmpValue /= base;
	} while(tmpValue > 0);


	if (pos > len)
			// the len parameter is invalid.
			return;

	if(zeros > pos){
		pBuf[zeros] = '\0';
		do{
			pBuf[d++ + (n ? 1 : 0)] = pAscii[0]; 
		}
		while(zeros > d + pos);
	}
	else
		pBuf[pos] = '\0';

	pos += d;
	do {
			pBuf[--pos] = pAscii[value % base];
			value /= base;
	} while(value > 0);
}

static void floatToString(float value, char *pBuf, uint32_t len, uint32_t base, uint8_t zeros, uint8_t precision){
	static const char* pAscii = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ";
	uint8_t start = 0xFF;
	if(len < 2)
		return;
	
	if (base < 2 || base > 36)
		return;
	
	if(zeros + precision + 1 > len)
		return;
	
	intToString((int64_t) value, pBuf, len, base, zeros);
	while(pBuf[++start] != '\0' && start < len); 

	if(start + precision + 1 > len)
		return;
	
	pBuf[start+precision+1] = '\0';
	
	if(value < 0)
		value = -value;
	pBuf[start++] = '.';
	while(precision-- > 0){
		value -= (uint32_t) value;
		value *= (float) base;
		pBuf[start++] = pAscii[(uint32_t) value];
	}
}


/*------------------------------------------------------------*/
// UART Interrupt
/*------------------------------------------------------------*/
void UART0_Handler(void){
	// Variaveis para leitura de dados da UART
	char m;
	
	// Enquanto o flag de recebimento nao for 
	while((UART0->FR & (1<<4)) != 0);
	UART0->FR &= (0<<4);
	
	UART0	->	RIS |= (1<<4);

	m = UART0 -> DR;
	
	// Seta a variavel de interrupt
	UART_interrupt = true;
}
/*------------------------------------------------------------*/
// Init perifericos
/*------------------------------------------------------------*/
void init_all(){
	cfaf128x128x16Init();
	// Inicializa��o da UART
	inicia_UART();
	initScreen();
	led_init();
	DWT_Config();
	servo_init();
}
/*------------------------------------------------------------*/
// Init threads
/*------------------------------------------------------------*/
int Init_Thread (void) {
	
	gera_fibonacci_ID = osThreadCreate (osThread(gera_fibonacci), NULL);
	if (!gera_fibonacci_ID) return(-1);
	
	uart_ID = osThreadCreate (osThread(uart), NULL);
	if (!uart_ID) return(-1);
	
	gera_primo_ID= osThreadCreate (osThread(gera_primo), NULL);
	if (!gera_primo_ID) return(-1);

	manipulador_ID = osThreadCreate (osThread(manipulador),NULL);
	if (!manipulador_ID) return(-1);
	
	return(0);
}
/**************************************************************/
/*************************************************************/
/*************************************************************/
/*------------------------------------------------------------*/
// THREAD ESCALONADOR
/*------------------------------------------------------------*/ 
void escalonador(void const *argument){

	// Variaiveis de controle
	uint32_t multiplo_manipulador;
	uint32_t multiplo_fibonacci;
	uint32_t multiplo_primo;
	
	// Variavel de tempo
	uint64_t tempo_atual;
	
	//Incremento do vetor
	uint8_t i;
	
	// Variaveis de manuseio do delta_l
	uint8_t thread_escolhida;
	uint64_t menor_delta_l=0xFFFFFFFF;
	
	// Set de prioridade
	osStatus status_prio;
	
	// Inicializacao das threads
	
	// Set de prioridades
	thread[MANIPULADOR].prioridade = PRIORIDADE_MAN ;
	thread[PRIMO].prioridade = PRIORIDADE_PRIMO;
	thread[FIBO].prioridade = PRIORIDADE_FIBO;
	thread[UART].prioridade = PRIORIDADE_UART;
	
	// Seta estado de nao criada
	thread[MANIPULADOR].estado = NAO_CRIADA ;
	thread[PRIMO].estado = NAO_CRIADA;
	thread[FIBO].estado = NAO_CRIADA;
	thread[UART].estado = NAO_CRIADA;
		
	// Seta status individuais
	thread[MANIPULADOR].end_status = NH ;
	thread[PRIMO].end_status = NH;
	thread[FIBO].end_status = NH;
	thread[UART].end_status = NH;	
		
	// Set de delta de relaxamento
	thread[MANIPULADOR].delta_l = 0 ;
	thread[PRIMO].delta_l = 0;
	thread[FIBO].delta_l = 0;
	thread[UART].delta_l = 0;

	while(true){
		// Espera pela flag
		osSignalWait(0x01,osWaitForever);
		osSignalClear(escalonador_ID,0x01);
		
//		#if GANTT == 1
//		osMutexWait(mutex_UART_ID,osWaitForever);
//		UARTprintstring("Escalonador: active,");
//		cycles = osKernelSysTick();
//		intToString(cycles,cycles_char,30,10,0);
//		UARTprintstring(cycles_char);
//		UARTprintstring(",");
//		osMutexRelease(mutex_UART_ID);
//		#endif
		
		// Nao e necessario, a priori
		osSignalClear(gera_primo_ID,0X01);
		osSignalClear(gera_fibonacci_ID,0X01);
		osSignalClear(manipulador_ID,0X01);
		osSignalClear(uart_ID,0X01);
		
		// Ajusta o tick inicial
		tempo_atual = osKernelSysTick();
		
		//**************************************************
		// THREAD FIBONACCI
		//**************************************************		
		// Incrementa o manipulador - Se 200, passou 1 segundo
		multiplo_fibonacci++;
		
		// Se multiplo do periodo
		if(multiplo_fibonacci == PERIODO_FIBO/TIMER_TICK){
			multiplo_fibonacci = 0;
			// Cria thread do fibonacci
			#if IDEIA_CRIA_THRED == 1 
			gera_fibonacci_ID = osThreadCreate(osThread(gera_fibonacci), NULL);
			if (!gera_fibonacci_ID){ 
				exit(0);
				UARTprintstring("Abort fibo!");
			}
			#endif 
			// Seta informacoes no vetor de threads
			thread[FIBO].id = gera_fibonacci_ID;
			// Thread estado
			thread[FIBO].estado = PRONTA;
			// Thread tempo inicio
			thread[FIBO].tempo_inicio = tempo_atual;
			// Thread prazo_estimado
			thread[FIBO].prazo_estimado = tempo_atual + EXECUCAO_FIBO;
			// Thread Deadline
			thread[FIBO].deadline = 1.5*thread[FIBO].prazo_estimado;
			// Thread Deadline
			thread[FIBO].delta_l = thread[FIBO].deadline - thread[FIBO].prazo_estimado;
		}

		//**************************************************
		// THREAD PRIMO
		//**************************************************
		// Incrementa o manipulador - Se 40, passou 0,2 segundo		
		multiplo_primo++;
		
		// Se multiplo do periodo
		if(multiplo_primo == PERIODO_PRIMO/TIMER_TICK){
			multiplo_primo=0;
			// Cria thread do escalonador		
			#if IDEIA_CRIA_THRED == 1 
			gera_primo_ID = osThreadCreate(osThread(gera_primo), NULL);
			if (!gera_primo_ID){ 
				exit(0);
				UARTprintstring("Abort primo!");
			}
			#endif
			// Seta informacoes no vetor de threads
			thread[PRIMO].id = gera_primo_ID;
			// Thread estado
			thread[PRIMO].estado = PRONTA;
			// Thread tempo inicio
			thread[PRIMO].tempo_inicio = tempo_atual;
			// Thread prazo_estimado
			thread[PRIMO].prazo_estimado = tempo_atual + EXECUCAO_FIBO;
			// Thread Deadline
			thread[PRIMO].deadline = 1.3*thread[PRIMO].prazo_estimado;
			// Thread Deadline
			thread[PRIMO].delta_l = thread[PRIMO].deadline - thread[PRIMO].prazo_estimado;
		}
		
		//**************************************************
		// THREAD MANIPULADOR
		//**************************************************
		// Incrementa o manipulador - Se 2, passou 0,1 segundo		
		multiplo_manipulador++;
		
		// Se multiplo do periodo
		if(multiplo_manipulador == PERIODO_MAN/TIMER_TICK){
			multiplo_manipulador=0;
			// Cria thread do escalonador			
			#if IDEIA_CRIA_THRED == 1 
			manipulador_ID = osThreadCreate(osThread(manipulador), NULL);
			if (!manipulador_ID){ 
				exit(0);
				UARTprintstring("Abort manipula!");
			}
			#endif
			// Seta informacoes no vetor de threads
			thread[MANIPULADOR].id = manipulador_ID;
			// Thread estado
			thread[MANIPULADOR].estado = PRONTA;
			// Thread tempo inicio
			thread[MANIPULADOR].tempo_inicio = tempo_atual;
			// Thread prazo_estimado
			thread[MANIPULADOR].prazo_estimado = tempo_atual + EXECUCAO_FIBO;
			// Thread Deadline
			thread[MANIPULADOR].deadline = 1.7*thread[MANIPULADOR].prazo_estimado;
			// Thread Deadline
			thread[MANIPULADOR].delta_l = thread[MANIPULADOR].deadline - thread[MANIPULADOR].prazo_estimado;
		}
		
		//**************************************************
		// THREAD UART
		//**************************************************
		// Dependo de um sinal de interrupcao		
		if(UART_interrupt == true){
			UART_interrupt = false;
			// Cria thread do escalonador			
			#if IDEIA_CRIA_THRED == 1 
			manipulador_ID = osThreadCreate(osThread(manipulador), NULL);
			if (!manipulador_ID){ 
				exit(0);
				UARTprintstring("Abort manipula!");
			}
			#endif
			// Seta informacoes no vetor de threads
			thread[UART].id = uart_ID;
			// Thread estado
			thread[UART].estado = PRONTA;
			// Thread tempo inicio
			thread[UART].tempo_inicio = tempo_atual;
			// Thread prazo_estimado
			thread[UART].prazo_estimado = tempo_atual + EXECUCAO_UART;
			// Thread Deadline
			thread[UART].deadline = 1.1*thread[UART].prazo_estimado;
			// Thread Deadline
			thread[UART].delta_l = thread[UART].deadline - thread[UART].prazo_estimado;				
		}

		//==================================================
		//==================================================
		//==================================================
		
		//**************************************************
		// ESCALONA TAREFAS JA EXISTENTES
		//**************************************************
	
		// Atualiza o valor
		menor_delta_l=0xFFFFFFFF;
		
		for(i=0;i<N_THREADS;i++){
			
			/* Calculo do deadline */
			// Verifica Deltas	
			if(thread[i].estado == SUSPENSA || thread[i].estado == PRONTA){
				// Somo com o tempo atual, por ser o tempo de suspensao
				thread[i].prazo_estimado += TIMER_CICLOS;
				// Recalculo de delta_l
				thread[i].delta_l = thread[i].deadline - thread[i].prazo_estimado;
			}
			
			/*Ajustes de PRIORIDADE*/
			// Ajust de prioridade de acordo com deadline
			// Feito para threads que nao sao realtime - Manipulador, UART e Fibonacci
			if(thread[i].estado == ENCERRADA){
				// Checa o status de finalizacao
				if(thread[i].end_status == SECONDARY_FAULT_1){
					thread[i].end_status = NH;
					//Decrementa a prioridade 
					thread[i].prioridade-=AJUSTE_PRIORIDADE;
				// Checa o status de finalizacao
				}
				else
				if(thread[i].end_status == SECONDARY_FAULT_2){
					thread[i].end_status = NH;
					//Decrementa a prioridade 
					thread[i].prioridade+=AJUSTE_PRIORIDADE;
				}
				else
				if(thread[i].end_status == MASTER_FAULT){
					thread[i].end_status = NH;
					UARTprintstring("\r\n--------- MASTER FAULT ---------\r\n");
					exit(0);
				}
			}
					
			/*Ajustes de Thread EXECUTANDO*/
			// Ajuste de qual thread ira receber o processador
			// Feito com osThreadSetPriority(ID,osPriorityAboveNormal);
			// Verifica se thread esta pronta ou estav
			if(thread[i].estado == PRONTA || thread[i].estado == EXECUTANDO){	
				// Seta prioridade como normal para todas
				//if(osThreadSetPriority(thread[i].id,osPriorityNormal) != osOK)
				//	UARTprintstring("Set priority nao okay");
				osSignalClear(thread[i].id,0x01);
				
				// Compara o delta l pra escolhar a de maior prioridade
				if(thread[i].delta_l < menor_delta_l){
					//Atualizo o valor
					menor_delta_l = thread[i].delta_l;
					// Escolho a thread para ser executada
					thread_escolhida = i;
				}
				// Se for houver igualdade, comparar por prioridade
				else
				if(thread[i].delta_l == menor_delta_l){
					if(thread[i].prioridade < thread[thread_escolhida].prioridade){
						//Atualizo o valor
						menor_delta_l = thread[i].delta_l;
						// Escolho a thread para ser executada
						thread_escolhida = i;
					}
				}
			}			
			
		} // Fim do La�o
		
		if( thread[thread_escolhida].estado == PRONTA ){
			// Thread escolhida tem alta prioridade no retorno ao processador
			//status_prio = osThreadSetPriority(thread[thread_escolhida].id,osPriorityAboveNormal);
			osSignalSet(thread[thread_escolhida].id,0x01);
			
			//if(status_prio == osErrorParameter)
			//	UARTprintstring("osErrorParameter");
			//else
			//if(status_prio == osErrorValue)
			//	UARTprintstring("osErrorValue");
			//else			
			//if(status_prio == osErrorResource)
			//	UARTprintstring("osErrorResource");
			
			thread[thread_escolhida].estado = EXECUTANDO;
		}

//		#if GANTT == 1
//		osMutexWait(mutex_UART_ID,osWaitForever);
//		cycles = osKernelSysTick();
//		intToString(cycles,cycles_char,30,10,0);
//		UARTprintstring(cycles_char);
//		UARTprintstring("\r\n");
//		osMutexRelease(mutex_UART_ID);
//		#endif		
	}

		
}
// THREAD UART
/*------------------------------------------------------------*/
void uart(void const *argument){
	// Variaveis para leitura de dados da UART
	uint32_t tempo_final;
	int i;
	
	while(true){
		osSignalWait(0x01,osWaitForever);
		osSignalClear(uart_ID,0x01);

		#if GANTT == 1
		osMutexWait(mutex_UART_ID,osWaitForever);
		UARTprintstring("UART: active,");
		cycles = osKernelSysTick();
		intToString(cycles,cycles_char,30,10,0);
		UARTprintstring(cycles_char);
		UARTprintstring(",");
		osMutexRelease(mutex_UART_ID);
		#endif		

		// Seta a flag para trocar de peca
		mudar_peca = !mudar_peca;
		
		/**************************************/
		// Seta informacoes no vetor de threads
		/**************************************/
		tempo_final = osKernelSysTick();
		// Thread inativa 
		thread[UART].estado = ENCERRADA;
		// Thread tempo fim
		if( tempo_final >= thread[PRIMO].deadline)
				thread[UART].end_status = SECONDARY_FAULT_1;
		else
		if( tempo_final < thread[PRIMO].deadline - thread[PRIMO].delta_l/2)
				thread[UART].end_status = SECONDARY_FAULT_2;
		
		//Volta ao escalonador
		osSignalSet(escalonador_ID,0x01);
		
		#if GANTT == 1
		osMutexWait(mutex_UART_ID,osWaitForever);
		cycles = osKernelSysTick();
		intToString(cycles,cycles_char,30,10,0);
		UARTprintstring(cycles_char);
		UARTprintstring("\r\n");
		osMutexRelease(mutex_UART_ID);
		#endif	
	}

	
}
/*------------------------------------------------------------*/
// THREAD GERA_FIBONACCI
/*------------------------------------------------------------*/
void gera_fibonacci(void const *argument){
	
	// Fibonacci
	int64_t n;
	
	// Variaveis de controle
	uint32_t tempo_final;
	uint64_t fibonacci_n;
	float num_ouro = 1.618034;
	

	while(true){
		osSignalWait(0x01,osWaitForever);
		osSignalClear(gera_fibonacci_ID,0x01);
		
		#if GANTT == 1
		osMutexWait(mutex_UART_ID,osWaitForever);
		UARTprintstring("gera_fibonacci: active,");
		cycles = osKernelSysTick();
		intToString(cycles,cycles_char,30,10,0);
		UARTprintstring(cycles_char);
		UARTprintstring(",");
		osMutexRelease(mutex_UART_ID);
		#endif
		
		//Fonte do calculo: https://www.mathsisfun.com/numbers/fibonacci-sequence.html
		
		// Calcula fibonacci utilizando a seguinte formula
		// Onde: 
		// X - Numero de ouro = 1.618034
		// Formula:
		// Fib(n) = (X^n-(1-X)^n)/sqrt(5)
		
		// ******** Teste de valores 
		
		// Tempo inicial de operacao
		cycles_ini = osKernelSysTick();
		
		// Gera os numeros de fibonacci com uma conta utilizando o numero de ouro
		n++;
		fibonacci_n = (uint32_t)((pow(num_ouro,n)-pow(1-num_ouro,n))/sqrt(5));
		
		// Transforma numero para string
		intToString(fibonacci_n,n_string,20,10,0);
		
		#if SIMULADOR == 0
		// Printa na UART o numero
		UARTprintstring("Numero de fibonacci:");
		UARTprintstring(n_string);
		UARTprintstring("\r\n");
		#endif
		
		// Mede o tempo fim da operacao
		cycles_fim = osKernelSysTick();
		delta_cycle = cycles_fim - cycles_ini;
	//	tempo = (float)delta_cycle/(float)osKernelSysTickFrequency;
	//	tempo*=1000;
		
	//	floatToString(tempo,n_string,20,10,0,10);
	//	UARTprintstring(n_string);
	//	UARTprintstring("us\r\n");

		/**************************************/
		// Seta informacoes no vetor de threads
		/**************************************/
		tempo_final = osKernelSysTick();
		// Thread inativa 
		thread[FIBO].estado = ENCERRADA;
		// Thread tempo fim
		if( tempo_final >= thread[PRIMO].deadline)
				thread[FIBO].end_status = SECONDARY_FAULT_1;
		else
		if( tempo_final < thread[PRIMO].deadline - thread[PRIMO].delta_l/2)
				thread[FIBO].end_status = SECONDARY_FAULT_2;
		
		//Volta ao escalonador
		osSignalSet(escalonador_ID,0x01);
		
		#if GANTT == 1
		osMutexWait(mutex_UART_ID,osWaitForever);
		cycles = osKernelSysTick();
		intToString(cycles,cycles_char,30,10,0);
		UARTprintstring(cycles_char);
		UARTprintstring("\r\n");
		osMutexRelease(mutex_UART_ID);
		#endif
	}
}
/*------------------------------------------------------------*/
// THREAD GERA_PRIMO
/*------------------------------------------------------------*/
void gera_primo(void const *argument){
	
	//Primo
	uint64_t number=2;
	// Variaveis de controle
	uint32_t tempo_final;

	while(true){
		osSignalWait(0x01,osWaitForever);
		osSignalClear(gera_primo_ID,0x01);

		#if GANTT == 1
		osMutexWait(mutex_UART_ID,osWaitForever);
		UARTprintstring("gera_primo: active,");
		cycles = osKernelSysTick();
		intToString(cycles,cycles_char,30,10,0);
		UARTprintstring(cycles_char);
		UARTprintstring(",");
		osMutexRelease(mutex_UART_ID);
		#endif
		
		// Tempo inicial de operacao
		cycles_ini = osKernelSysTick();	
		
		//Verifica se e primo
		while(!isPrime(number)){
			number++;
		}
		// Transforma numero para string
		intToString(number,n_string,20,10,0);
		
		#if SIMULADOR == 0 
		// Printa na UART o numero
		UARTprintstring("Numero de primo:");
		UARTprintstring(n_string);
		UARTprintstring("\r\n");
		#endif
			
		// Garante a nao repeticao do numero
		number++;
		
		// Mede o tempo fim da operacao
		cycles_fim = osKernelSysTick();
		delta_cycle = cycles_fim - cycles_ini;
	//	tempo = (float)delta_cycle/(float)osKernelSysTickFrequency;
	//	tempo*=1000;
		
	//	// Printa tempo final na tela
	//	floatToString(tempo,n_string,20,10,0,10);
	//	UARTprintstring(n_string);
	//	UARTprintstring("us\r\n");
		
		/**************************************/
		// Seta informacoes no vetor de threads
		/**************************************/
		tempo_final = osKernelSysTick();
		// Thread inativa 
		thread[PRIMO].estado = ENCERRADA;
		// Check de tempo_final
		if(tempo_final > thread[PRIMO].deadline)
			thread[PRIMO].end_status = MASTER_FAULT;
		
		//Volta ao escalonador
		osSignalSet(escalonador_ID,0x01);
		
		#if GANTT == 1
		osMutexWait(mutex_UART_ID,osWaitForever);
		cycles = osKernelSysTick();
		intToString(cycles,cycles_char,30,10,0);
		UARTprintstring(cycles_char);
		UARTprintstring("\r\n");
		osMutexRelease(mutex_UART_ID);
		#endif
	}
}
/*------------------------------------------------------------*/
// THREAD MANIPULADOR
/*------------------------------------------------------------*/
void manipulador(void const *argument){
	
	//Variaveis de controle
	uint32_t tempo_final;
	uint8_t  movimento=3,movimento_retorno;
	
	bool terminou_movimento = false;
	bool alto_azul = true;
	bool alto_vermelho = true;
	bool troca_peca = false;
	
	uint32_t max_2[11] = {32500,35750,39000,42250,45500,48750,52000,55250,58500,61750,65000};
	uint32_t max_3[11] = {21667,26000,30333,34667,39000,43333,47667,52000,56333,60667,65000};
	uint32_t max_4[11] = {16250,21125,26000,30875,35750,40625,45500,50375,55250,60125,65000};
	uint32_t max_4_2[11] = {16250,21125,26000,30875,35750,40625,45500,50375,50375,50375,50375};
	
	int8_t toggle = 0;
	int8_t index = 0;
		
	servo_write0(MAX/2);
	servo_write1(16250);
	servo_write2(65000);
	servo_write3(21667);
	
	while(true){
		osSignalWait(0x01,osWaitForever);
		osSignalClear(manipulador_ID,0x01);
		
		#if GANTT == 1
		osMutexWait(mutex_UART_ID,osWaitForever);
		UARTprintstring("manipulador: active,");
		cycles = osKernelSysTick();
		intToString(cycles,cycles_char,30,10,0);
		UARTprintstring(cycles_char);
		UARTprintstring(",");
		osMutexRelease(mutex_UART_ID);
		#endif

		#if SIMULADOR == 0
		//servo_write0(0); // Base
		//servo_write1(0); // Horizontal
		//servo_write2(0); // Elevacao
		//servo_write3(0); // Garra
				
		if(terminou_movimento == false){
			// Movimentacao - Bra�o frente ------------ ALTO
			if(movimento == 0){
				// Move a garra na medida desejada
				if(!mudar_peca)
					servo_write1(max_4[toggle]);
				else
					servo_write1(max_4_2[toggle]);
				toggle++;
				if (toggle == 10){
					toggle = 10;
					movimento++;
				}
			}
			else
			// Movimentacao - Abre garra ------------ ALTO
			if(movimento == 1){
				// Move a garra na medida desejada
				servo_write3(max_3[toggle]);
				toggle--;
				if (toggle == -1){
					toggle = 10;
					movimento++;
				}
			}
			else
			if(movimento == 2){
				// Atraso ------------ ALTO
				if(!mudar_peca)
					servo_write1(max_4[toggle]);
				else
					servo_write1(max_4_2[toggle]);
				toggle--;
				if (toggle == -1){
					// Indica termino de movimentacao
					terminou_movimento = true; // Deixa pe�a em cima		
					toggle = 0;
					movimento++;
				}
			}
			else
			if(movimento == 3){
				// Atraso ------------ ALTO
				if(!mudar_peca)
					servo_write1(max_4[toggle]);
				else
					servo_write1(max_4_2[toggle]);
				toggle++;
				if (toggle == 10){
					toggle = 0;
					movimento++;
				}
			}			
			else
			// Movimentacao - Fecha garra ------------ ALTO
			if(movimento == 4){
				// Move a garra na medida desejada
				servo_write3(max_3[toggle]);
				toggle++;
				if (toggle == 10){
					toggle = 10;
					movimento++;
				}
			}		
			else
			// Movimentacao - Tras ------------ ALTO
			if(movimento == 5){
				// Move a garra na medida desejada
				if(!mudar_peca)
					servo_write1(max_4[toggle]);
				else
					servo_write1(max_4_2[toggle]);
				toggle--;
				if (toggle == -1){
					toggle = 10;
					movimento++;
				}
			}
			else
			// Movimentacao - Ajusta para abaixar ------------ ALTO
			if(movimento == 6){
				// Move a garra na medida desejada
				servo_write2(max_3[toggle]);
				toggle--;
				if (toggle == -1){
					toggle = 0;
					movimento++;
				}
			}		
			else
			// Movimentacao - Frente ------------ BAIXO
			if(movimento == 7){
				// Move a garra na medida desejada
					servo_write1(max_4[toggle]);
				toggle++;
				if (toggle == 10){
					toggle = 10;
					movimento++;
				}
			}		
			else
			// Movimentacao - Abre garra ------------ BAIXO
			if(movimento == 8){
				// Move a garra na medida desejada
				servo_write3(max_3[toggle]);
				toggle--;
				if (toggle == -1){
					toggle = 0;
					movimento++;
				}
			}
			else
			if(movimento == 9){
				// Atraso ------------ BAIXO
					servo_write1(max_3[toggle]);
				toggle--;
				if (toggle == -1){
					terminou_movimento = true;	// Deixa pe�a em baixo
					toggle = 0;
					movimento++;
				}
			}
			else
			if(movimento == 10){
				// Atraso ------------ BAIXO
					servo_write1(max_3[toggle]);
				toggle++;
				if (toggle == 10){
					toggle = 0;
					movimento++;
				}
			}				
			else
			// Movimentacao - Fecha garra ------------ BAIXO
			if(movimento == 11){
				// Move a garra na medida desejada
				servo_write3(max_3[toggle]); // Pega a tampa
				toggle++;
				if (toggle == 10){
					toggle = 10;
					movimento++;
				}
			}
			else
			// Movimentacao - Tr�s
			if(movimento == 12){
				// Move a garra na medida desejada
					servo_write1(max_4[toggle]);
				toggle--;
				if (toggle == -1){
					toggle = 0;
					movimento++;
				}
			}
			else
			// Movimentacao - Levanta
			if(movimento == 13){
				// Move a garra na medida desejada
				servo_write2(max_3[toggle]);
				toggle++;
				if (toggle == 10){
					toggle = 0;
					movimento = 0;
				}
			}
		}
	
		// Checa por movimento terminado
		if(terminou_movimento == true){
			terminou_movimento = false;
			
			// Giro o bra�o
			if(mudar_peca == false){
				servo_write0(MAX/2); // Vermelho
			}
			else{
				servo_write0(MAX/5); // Azul
			}
		}		
		#endif
		
		/**************************************/
		// Seta informacoes no vetor de threads
		/**************************************/
		tempo_final = osKernelSysTick();
		// Thread inativa 
		thread[MANIPULADOR].estado = ENCERRADA;
		// Thread tempo fim
		if( tempo_final >= thread[PRIMO].deadline)
				thread[MANIPULADOR].end_status = SECONDARY_FAULT_1;
		else
		if( tempo_final < thread[PRIMO].deadline - thread[PRIMO].delta_l/2)
				thread[MANIPULADOR].end_status = SECONDARY_FAULT_2;
		
		//Volta ao escalonador
		osSignalSet(escalonador_ID,0x01);

		#if GANTT == 1
		osMutexWait(mutex_UART_ID,osWaitForever);
		cycles = osKernelSysTick();
		intToString(cycles,cycles_char,30,10,0);
		UARTprintstring(cycles_char);
		UARTprintstring("\r\n");
		osMutexRelease(mutex_UART_ID);
		#endif
	}
}
/*------------------------------------------------------------*/
// MAIN
/*------------------------------------------------------------*/
int main() {
	// Inicializa o Kernel
	osKernelInitialize();	
	
	#if SIMULADOR == 0
	//Initializing all peripherals
	init_all();
	#endif
	
	SystemCoreClockUpdate();
	// Inicializa as Threads
	if(Init_Thread()==-1)
		exit(0);
		// Mensagens de erro de inicializa��o
	
	// Cria thread do escalonador
	escalonador_ID = osThreadCreate(osThread(escalonador), NULL);
	if (!escalonador_ID) 
		exit(0);
	
	// Inicializa o Kernel, junto com as threads
	osKernelStart();

	mutex_UART_ID = osMutexCreate(osMutex(uart_mutex));
	
	Timer_ID = osTimerCreate(osTimer(Timer), osTimerPeriodic, (void *)0);      // creates a one-shot timer;

	osTimerStart(Timer_ID,TIMER_TICK);
	
	//Main aguarda para sempre
	osDelay(osWaitForever);

}
